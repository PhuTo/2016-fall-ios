//
//  ViewController.swift
//  CalcIt
//
//  Created by mobile apps on 2016-11-05.
//  Copyright © 2016 mobile apps. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tfFirstNumber: UITextField!
    @IBOutlet weak var tfSecondNumber: UITextField!
    @IBOutlet weak var lblMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func btnAdd(sender: AnyObject) {
        let firstNumber = tfFirstNumber.text!
        let secondNumber = tfSecondNumber.text!
        if (firstNumber.isEmpty) && (secondNumber.isEmpty)
        {
            lblMessage.text = "Please provide 2 numbers"
            return
        }
        else if firstNumber.isEmpty {
            lblMessage.text = "Please provide first number"
            return
        }
        else if secondNumber.isEmpty {
            lblMessage.text = "Please provide second number"
            return
        }
        else
            if let firstNumberFloat = Float64(firstNumber)
            {
             if let secondNumberFloat = Float64(secondNumber)
            {
                //lblMessage = String(firstNumberFloat + secondNumberFloat)
                //lblMessage = ToString(firstNumberFloat + secondNumberFloat)
                let result = firstNumberFloat + secondNumberFloat;
                lblMessage.text = " \(result)"
                }
        }
    }
        @IBAction func btnSubtract(sender: AnyObject) {
            Validate(tfFirstNumber.text!, secondOperand: tfSecondNumber.text!, Operator: "-")
        }
        
        @IBAction func btnMultiply(sender: AnyObject) {
        }
        
        @IBAction func btnDivisiom(sender: AnyObject) {
        }

        func Validate (firstOperand: String, secondOperand: String, Operator: String)
        {
            //let firstNumber = tfFirstNumber.text!
            //let secondNumber = tfSecondNumber.text!
            if (firstOperand.isEmpty) && (secondOperand.isEmpty)
            {
                lblMessage.text = "Please provide 2 numbers"
                return
            }
            else if firstOperand.isEmpty {
                lblMessage.text = "Please provide first number"
                return
            }
            else if secondOperand.isEmpty {
                lblMessage.text = "Please provide second number"
                return
            }
            else
                if let firstNumberFloat = Float64(firstOperand)
                {
                    if let secondNumberFloat = Float64(secondOperand)
                    {
                        //lblMessage = String(firstNumberFloat + secondNumberFloat)
                        //lblMessage = ToString(firstNumberFloat + secondNumberFloat)
                        switch Operator {
                          case "+":
                            let result = firstNumberFloat + secondNumberFloat;
                            lblMessage.text = " \(result)"
    
                        case "-":
    let result = firstNumberFloat - secondNumberFloat;
    lblMessage.text = " \(result)"
                            
                        case "*":
                                let result = firstNumberFloat * secondNumberFloat;
                                lblMessage.text = " \(result)"
                        
                        case "/":
                        let result = firstNumberFloat / secondNumberFloat;
                        lblMessage.text = " \(result)"
                        
                        default:
                        lblMessage.text = " Unexpected error encountered"
                    }
            }
            }
    }
}



    
    
    
    
    



