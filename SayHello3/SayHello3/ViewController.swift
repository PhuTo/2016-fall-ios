//
//  ViewController.swift
//  SayHello3
//
//  Created by mobile apps on 2016-11-05.
//  Copyright © 2016 mobile apps. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblHello: UILabel!
    @IBOutlet weak var tfName: UITextField!
    
    @IBOutlet weak var tfAge: UILabel!
    
    @IBOutlet weak var tfAge2: UITextField!
    
    @IBAction func btSayHelloClick(sender: AnyObject) {
        let name = tfName.text!
        let age = tfAge2.text!
        if name.isEmpty || age.isEmpty
        {
            lblHello.text = "Please provide your name"
            return
        }
        
        if (name == "Santa")
        {
            lblHello.text = "Hello (name)! Nice to meet you!"        }
        else
        {
            if let ageInt = Int(age) {
            // lblHello.text = "Hello \(name)! Nice to meet you!"
            lblHello.text = "Hello \(name), you are \(ageInt) years old"
            }
            else
            {
                lblHello.text = "Age must be an integer"
            }
            }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

